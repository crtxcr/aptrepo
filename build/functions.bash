#!/bin/bash

function newest_git_tag()
{
	git tag --sort="-version:refname" | head -n 1
}

function build_git_repo()
{
	BUILDDIR="$1"
	REPOURL="$2"
	NAME="$3"
	VERSION="$4"
	TARGET_DIR="$5"
	cp -R debian "$BUILDDIR"
	cp ../CHANGELOG_TEMPLATE "$BUILDDIR"/debian/changelog
	cd "$BUILDDIR"
	git clone "$REPOURL" "$NAME"
	cd "$NAME"
	if [ "$VERSION" = "_auto_" ] ; then
		VERSION=$(newest_git_tag)
	fi
	
	if [ -n "$VERSION" ] ; then
		git verify-tag "$VERSION"
		git checkout "$VERSION"
	else
		LAST_COMMIT_ID=$(git log --pretty="format:%h" -n1)
		VERSION="0.1+git$LAST_COMMIT_ID"
	fi
	mv ../debian/ .
	VERSION_CHANGELOG=$( echo $VERSION | sed -e 's/v//g')
	sed -e "s/NAME_PLACEHOLDER/$NAME/g" -i ./debian/changelog
	sed -e "s/VERSION_PLACEHOLDER/$VERSION_CHANGELOG/g" -i ./debian/changelog
	DATE_CHANGELOG=$(date -R)
	sed -e "s/DATE_PLACEHOLDER/$DATE_CHANGELOG/g" -i ./debian/changelog

	git submodule init
	git submodule update
	dpkg-buildpackage --no-sign
	cp -a ../*.deb "$TARGET_DIR"/
}
