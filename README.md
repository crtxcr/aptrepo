apt repository for quitesimple.org projects
===========================================
This follows a rather pragmatic approach.

Some inspiration was taken from https://manuel-io.github.io/blog/2017/07/09/creating-an-apt-repository/, thanks!

Hosted at https://repo.quitesimple.org/debian/
